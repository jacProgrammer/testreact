let data = [
  {
    view1: {
      id: "register",
      name: "register",
      props: {
        id: "form",
        title: "Crear Cuenta",
        sub_title:"Es gratis y lo será siempre.",
        type: "form",
        content: {
          tagTipe: {
            input: {
              input_name: {
                tag: "InputText",
                label: "",
                name:"name",
                class: "input_prymary",
                type: "text",
                placeholder: "Nombre",
                "min_lenght" : 2,
                max_lenght: 19,
                required: true,
                tool_tip: "¿Como te llamas?",
                value:""
              },
              input_phone: {
                label: "",
                name:"phone",
                class: "input_prymary",
                type: "text",
                placeholder: "(xx) xxxx-xxxx",
                min_lenght : 10,
                max_lenght: 10,
                required: true,
                tool_tip: "¿Cuál es tu número de teléfono?",
                value:""
              },
              input_email: {
                label: "",
                name:"email",
                class: "input_prymary",
                type: "email",
                placeholder: "Correo electronico",
                "min_lenght" : 10,
                max_lenght: 50,
                required: true,
                tool_tip: "Usaras ésta información cuando entres a tu cuenta ",
                value:""
              },
              input_password: {
                label: "",
                name:"password",
                class: "input_prymary",
                type: "password",
                placeholder: "Contraseña nueva",
                "min_lenght" : 8,
                max_lenght: 16,
                required: true,
                tool_tip: "Ingresa una cmbinación de al menos seis nuemros, letras, signos de puntuación (como '!' y '&').",
                value:""
              }
            },
            button: {
              button_send: {
                text: "Registrarte",
                name: "submit_register",
                id: "submit_register",
                class: "btn_primary",
                type:"submit"
              }
            }
          },
        }
      }
    },
    view2: {
      id: 'list_post',
      name: 'list_post',
      props: {
        id: 'view',
        title: 'Post',
        type: 'view',
        content: {
          post1: {
            image:{
              url: 'url.com'
            },
            owner: {
              text: 'Owner'
            },
            description: {
              text: 'Description'
            },
          },
          post2: {
            image:{
              url: 'url.com'
            },
            owner: {
              text: 'Owner'
            },
            description: {
              text: 'Description'
            },
          },
          post3:{
            image:{
              url: 'url.com'
            },
            owner: {
              text: 'Owner'
            },
            description: {
              text: 'Description'
            },
          }

        }
      }

    },
    view3: {
      id: 'detail_post',
      name: 'detail_post',
      props: {
        id: 'view',
        title: 'Detalle del post',
        type: 'view',
        content: {
          content: {
            image:{
              url: 'url.com'
            },
            owner: {
              text: 'Owner'
            },
            description: {
              text: 'Description'
            },
  
          }
        }
      }
    }
  },
]

export default data