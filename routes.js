import { createStackNavigator, createAppContainer,createDrawerNavigator,createSwitchNavigator } from 'react-navigation';

import WelcomeContainer from './app/contaners/welcome/index';
import LoginContainer from './app/contaners/login/index';
import RegisterContainer from './app/contaners/register/index';

import SidebarContainer from './app/contaners/sideBar/index';
import HomeContainer from './app/contaners/home/index';


const initRoute = () => {
  return {
    Welcome: WelcomeContainer,
    Login: LoginContainer,
    Register: RegisterContainer
  }
};
const routeInternal =() => {
  return {
    Home: HomeContainer
  }

}

const AppInitNavigation = createStackNavigator(initRoute(), {initialRouteName: 'Welcome'})
const AppInternalNavigation = createDrawerNavigator(routeInternal(), {initialRouteName: 'Home', contentComponent:SidebarContainer})

export const Routes = createAppContainer(createSwitchNavigator(
    {
    Initial: AppInitNavigation,
    AppInternal: AppInternalNavigation
    },
  )
)