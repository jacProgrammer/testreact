import { StyleSheet } from "react-native"
import Color from "../../assets/style/colors"

const styles = StyleSheet.create({
  container: {
    backgroundColor: Color.PRIMARY,
  },
  btnStyle: {
    borderColor:'#FFF',
    borderRadius: 5
  },
  btnFoot: {
    borderTopColor: '#898989',
    borderTopWidth: 0.3
  },
  touchStyle: {
    textAlign:'center'
  },
  LabelTouchStyle : {
    textAlign:'center',
    fontWeight: 'bold',
    marginTop: 15,
    marginBottom: 15,

  },

});

export default styles