import React, {Component} from 'react';
import {Image} from 'react-native';
import BtnPrimary from '../../components/btnPrimary'
import TouchText from '../../components/touchText'
import Styles from './style'
import Img from '../../assets/images/images'

import { Container, Thumbnail, View, Text, Content} from 'native-base'

export default class WelcomeContainer extends Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      nameDefault: 'Nombre Guardado'
    }
  }
  goLogin= () => {
    this.props.navigation.navigate('Login')
  }
  goRegister= () => {
    this.props.navigation.navigate('Register')
  }
  render() {
    return (
      <Container style= {[Styles.container, {width:'100%', justifyContent: 'space-around', alignSelf:'center'}]}>
          <View  style={{justifyContent:'center', alignItems:'center',  marginTop:'auto'}}>
            <View style={{justifyContent: 'center', alignItems:'center'}}>
              <Image style={{width:200, height: 100, resizeMode: 'contain', alignSelf:'center' }} source={Img.instagramLogo}/>
              <Thumbnail large style={{height: 100, width: 100, borderRadius: 100/2}} source={Img.imgTest} />
            </View>
            <View style={{padding:30, justifyContent:'center', alignItems:'center'}}>
              <BtnPrimary label= {['iniciar sesión como ', this.state.nameDefault]} btnStyle={Styles.btnStyle}
                onPress={()=>this.props.navigation.navigate('Home')} >
              </BtnPrimary>
              <TouchText label={'Eliminar'} btnStyle= {Styles.touchStyle} labelStyle={Styles.LabelTouchStyle}/>
            </View>
          </View>

          <View style= {{width:'100%', alignItems:'center', marginTop:'auto'}}>
            <Text style={{color:'#898989', fontSize: 13, fontWeight:'bold', marginBottom: 10}}>
              Impressum/Terms/NetzDG
            </Text>
            <View style={{width:'100%', flexDirection:'row',}}>
              <View style= {{width:'50%',justifyContent:'center'}}>
                <TouchText onPress={this.goLogin} label={'Cambiar de cuenta'} btnStyle= {Styles.btnFoot} labelStyle={Styles.LabelTouchStyle}/>
              </View>
              <View style= {{width:'50%', justifyContent:'center', borderLeftColor:'#898989', borderLeftWidth:0.3}}>
                <TouchText onPress={this.goRegister} label={'Registrarte'} btnStyle= {Styles.btnFoot} labelStyle={Styles.LabelTouchStyle}/>
              </View>
            </View>
          </View>
        
      </Container>
    )
  }
} 