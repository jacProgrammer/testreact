import React, {Component} from 'react';
import {Image,TextInput, TouchableOpacity,} from 'react-native';
import BtnPrimary from '../../components/btnPrimary'
import TouchText from '../../components/touchText'
import InputPtimary from '../../components/inputPrimary'
import Styles from './style'
import Img from '../../assets/images/images'
import DataJson from '../../../dataJs'

import { Container, Icon, View, Text, Content, Thumbnail} from 'native-base'
import { TextInputMask } from 'react-native-masked-text'

export default class RegisterContainer extends Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      name: '',
      email: '',
      phone: '',
      password: '',
      hidePassword: true,
      validEmail: true,
      validPassword: true,
      validForm: true,
      data: {},
      dataInput: {

      }
    }
  }
  componentDidMount() {
    DataJson.forEach(data => {
      if(data.view1.id == 'register'){
        // console.log()
        
        let map = new Map(Object.entries(data.view1.props.content.tagTipe));
        console.log(map);
        
        if(Object.keys(data.view1.props.content.tagTipe) == 'input'){
          debugger
          let tagType =Object.entries(data.view1.props.content.tagTipe)
          tagType.forEach(typeTag => {
            console.log(typeTag)
            if(typeTag == 'input') {
            console.log(typeTag)
            }
          })
        }
      }
    });
  }
  goLogin= () => {
    this.props.navigation.navigate('Login')
  }

  managePasswordVisibility = () =>{
    this.setState({ hidePassword: !this.state.hidePassword });
  }

  handleChange=(field,value)=>{
    let {data}=this.state
     data[field]=value
     this.setState({data})
   }

  validateName= (value) => {
    this.setState({name:value})
  }

  validateEmail= (value) => {
    this.setState({email:value})
    let reg = /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/;
    let validate = value.match(reg);
    if (value && validate){
      this.setState({validEmail:false}) ;
    }
    else {
      this.setState({validEmail:true}) ;
    }

   }
  
  validatePhone= (value) => {
    this.setState({phone:value})
  }

  validatePassword= (value) => {
    this.setState({password:value})
    let re = /^(?=.*\d)(?=.*[A-Z])(?=.*[a-z])\S{8,16}$/
    let validate = value.match(re);
    if (value && validate){
      this.setState({validPassword:false}) ;
    }
    else {
      this.setState({validPassword:true}) ;
    }

  }

  onBlur= (value)=> {
  }
  validForm = () => {
    if(this.state.name && this.state.email && this.state.phone && this.state.password){
      return false
    }
    return true
  }
 

  render() {
    let {goLogin,
      validateName,
      validateEmail,
      validatePhone,
      validatePassword,
      onBlur} = this
    return (
      <Container style= {[Styles.container, {width:'100%', justifyContent: 'space-around', alignSelf:'center'}]}>
        <Content>

          <View  style={{justifyContent:'center', alignItems:'center',  marginTop:50, marginBottom: 10}}>
            
            <View style={{justifyContent: 'center', alignItems:'center'}}>
              <Thumbnail large style={{height: 150, width: 150, borderRadius: 100/2}} source={Img.profileEmpty} />
            </View>

            <View style={{width:'100%', padding: 30,}}>

              <View style={{marginBottom: 15, marginTop: 15}}>
                <InputPtimary
                  placeholder={'Nombre completo'}
                  value={this.state.name}
                  onChangeText={validateName}
                  onBlur={onBlur}
                />
                  
                {this.state.name < 2 && 
                  <Text style={{color: '#ccc5c5',fontSize: 10,}}>
                    Ingresa tu nombre completo
                  </Text>
                }
              </View>

              <View style={{marginBottom: 15, marginTop: 15}}>
                <InputPtimary
                  value={this.state.email}
                  keyboardType={'email-address'}  placeholder={'Ingresa tu email'}
                  onChangeText={validateEmail}
                  onBlur={onBlur}
                />
                {this.state.validEmail && 
                  <Text style={{color: '#ccc5c5',fontSize: 10,}}>
                    Ingresa un correo valido
                  </Text>
                }
              </View>

              <View style={{marginBottom: 15, marginTop: 15}}>
                <TextInputMask
                  maxLength={14}
                  placeholder='(xx) xxxx-xxxx'
                  style= {Styles.inputPrimary}
                  type={'cel-phone'}
                  value={this.state.phone}
                  includeRawValueInChangeText={true}
                  onChangeText={(text, result) => {this.setState({phone: result})
                  }}
                  // onBlur = {() => {this.validateInput(this.state.phone, 4)}}
                  // onEndEditing = {() => {this.validateInput(this.state.phone, 4)}}
                />
                {this.state.passValid && 
                  <Text style={{color: '#ccc5c5',fontSize: 10,}}>
                    La contraseña debe contener al menos un mayúscula, un minúscula, un número y un cracter especial( @, #, $)
                  </Text>
                }
              </View>

              <View style={{marginBottom: 15, marginTop: 15}}>
                <View style = { Styles.textBoxBtnHolder }>
                  <TextInput 
                    value={this.state.password}
                    placeholder={'Contraseña'} underlineColorAndroid = "transparent" 
                    secureTextEntry = { this.state.hidePassword } style = { Styles.textBox }
                    onChangeText={validatePassword}
                  />
                  
                  <TouchableOpacity activeOpacity = { 0.8 } style = { Styles.visibilityBtn } onPress = { this.managePasswordVisibility }>
                    <Icon name={this.state.hidePassword? 'eye':'eye-off'}/>
                  </TouchableOpacity>
                </View>
                {this.state.validPassword && 
                  <Text style={{color: '#ccc5c5',fontSize: 10,}}>
                    La contraseña debe contener al menos un mayúscula, un minúscula, un número y un cracter especial( @, #, $)
                  </Text>
                }
              </View>

              <BtnPrimary  disabled={this.state.name && this.state.email && this.state.phone && this.state.password? false : true} label= {['Registrar ', this.state.nameDefault]} 
                btnStyle={this.state.name && this.state.email && this.state.phone && this.state.password ? Styles.btnStyle: Styles.btnStyleDisabled } />
              <View style={{marginTop:30}}>
                <TouchText onPress={goLogin} label={'Iniciar sesión'} btnStyle= {Styles.touchStyle} labelStyle={Styles.LabelTouchStyle}/>
              </View>
            </View>
          
          </View>
        </Content>
      </Container>
    )
  }
} 