import { StyleSheet } from "react-native"
import Color from "../../assets/style/colors"


const style = StyleSheet.create({
  inputPrimary: {
    backgroundColor: '#FAFAFA',
    borderRadius: 5,
    borderWidth: 1,
    borderColor:'#ccc5c5'
    
  },
  textBoxBtnHolder:{
    position: 'relative',
    alignSelf: 'stretch',
    justifyContent: 'center'
  },
  textBox:{
    backgroundColor: '#FAFAFA',
    borderRadius: 5,
    borderWidth: 1,
    borderColor:'#ccc5c5',
  },
 
  visibilityBtn:{
    position: 'absolute',
    right: 3,
    height: 30,
    width: 30,
  },
  btnStyleDisabled: {
    opacity: 0.5
  },
  btnStyle: {
    opacity: 1
  },

})

export default style