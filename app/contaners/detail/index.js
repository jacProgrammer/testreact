import React, { Component } from 'react';
import { Image } from 'react-native';
import Img from '../../assets/images/images'
import {  Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body, Right } from 'native-base';
const CardComponent=({name, image, description})=>{
  return(
    <Card>
      <CardItem>
        <Left>
          <Thumbnail small source={Img.profileEmpty} />
          <Body>
            <Text>{name? name : 'Nombre Perfil'}</Text>
          </Body>
        </Left>
      </CardItem>
      <CardItem cardBody>
        <Image source={image?image: Img.cardImg} style={{height: 300, width: null, flex: 1}}/>
      </CardItem>
      <CardItem>
      <Body>
        <Text>{description?description: 'Lorem impsu short'} </Text>
      </Body>
      
      </CardItem>
    </Card>
  )

}
export default CardComponent