import React, {Component} from 'react';
import {Image, Button} from 'react-native';
import TouchText from '../../components/touchText'
import Styles from './style'
import Img from '../../assets/images/images'
import HeaderTemplate from '../../components/header'
import CardComponent from './cardComponent'
import BtnPrimary from '../../components/btnPrimary'


import { Container, View, Text, Content} from 'native-base'
import Modal from "react-native-modal";

export default class HomeContainer extends Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      List: [
        {
          name: 'Jac',
          img: Img.cardImg,
          description:'Lorem none data'
        },
        {
          name: 'Name',
          description:'Lorem none data'
        },
        {
          img: Img.cardImg,
        },
        
      ],
      isModalVisible: false
    }
  }
  goLogin= () => {
    this.props.navigation.navigate('Login')
  }
  goRegister= () => {
    this.props.navigation.navigate('Register')
  }
  drawerOpen=()=>{
    this.props.navigation.openDrawer();
  }
  toggleModal = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible });
  };
  render() {
    return (
      <Container style= {[Styles.container, {width:'100%', justifyContent: 'space-around', alignSelf:'center'}]}>
          <HeaderTemplate onPress={this.drawerOpen}/>

          <View  style={{justifyContent:'center', alignItems:'center'}}>
            <View style={{justifyContent: 'center', alignItems:'center'}}>
              <Image style={{width:100, height: 50, resizeMode: 'contain', alignSelf:'center' }} source={Img.instagramLogo}/>
            </View>
          </View>

          <Content style={{padding:20}}>
            <View style={{marginBottom: 30}}>
              {this.state.List.map((user =>
                <CardComponent
                name={user.name}
                image={user.img}
                description={user.description}
                key={user.index}
                />
                ))}
            </View>
          </Content>
          <BtnPrimary style={{backgroundColor:'red'}} label={"Show modal"} onPress={this.toggleModal} />
          <Modal isVisible={this.state.isModalVisible}>
            <View style={{ flex: 1 }}>
              <Text>Hello!</Text>
              <Button title="Hide modal" onPress={this.toggleModal} />
            </View>
          </Modal>
      </Container>
    )
  }
} 