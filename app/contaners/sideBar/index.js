import React, { Component } from 'react';
import {StyleSheet,View,Alert,AsyncStorage} from 'react-native';
import { Container, Header, Content, List, ListItem, Text,Body,Title,Left,Thumbnail,Right,Button,Icon } from 'native-base';
import {Image} from 'react-native';
import Img from '../../assets/images/images'


export default class Sidebar extends Component {


  render() {

    let{navigation}=this.props

    return (
        <Container style={{flex:1}}>

          <Header transparent style={{marginBottom:20, marginTop: 20}}>
            <Body style={{flex:1, justifyContent: 'center', alignItems:'center'}}>
              <Image style={{width:100, height: 100, resizeMode: 'contain', alignSelf:'center' }} source={Img.instagramLogo}/>
            </Body>
          </Header>
          <Content>

          <ListItem icon >
              <Left>
                  <Icon name={'home'} style={{color:'black'}}/>
              </Left>
              <Body>
                  <Text style={{color:'black'}}>{'Home'}</Text>
              </Body>
              <Right/>
          </ListItem>

          </Content>
          <View style={styles.boton}>
            <Button full bordered light>
                <Text>CERRAR SESIÓN</Text>
            </Button>
            <Text style={{fontSize:12, color:'white',marginTop:20 }}>Términos y condiciones</Text>
          </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({

  title:{
      width:'100%',
      color:'white',
      marginBottom:20,
  },
  texito:{
      color:'white'
  },
  boton:{
      padding:20,
      justifyContent:'center',
      alignItems:'center'
  }
})