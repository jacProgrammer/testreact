import { StyleSheet } from "react-native"
import Color from "../../assets/style/colors"

const styles = StyleSheet.create({
  container: {
    backgroundColor: Color.PRIMARY,
  },
  btnStyleDisabled: {
    opacity: 0.5
  },
  btnStyle: {
    opacity: 1
  },
  btnFoot: {
    borderTopColor: '#898989',
    borderTopWidth: 0.3,
    fontWeight: 'bold'
  },
  touchStyle: {
  },
  LabelTouchStyle : {
    color: '#ccc5c5',
    fontSize: 15,
    fontWeight: 'bold',
    marginTop: 15,
    marginBottom: 15,

  },
  validPass: {
    color: '#ccc5c5',
    fontSize: 10,
  },

});

export default styles