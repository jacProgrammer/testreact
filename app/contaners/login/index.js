import React, {Component} from 'react';
import {Image,TextInput, TouchableOpacity, StyleSheet} from 'react-native';
import BtnPrimary from '../../components/btnPrimary'
import TouchText from '../../components/touchText'
import InputPtimary from '../../components/inputPrimary'
import Styles from './style'
import Img from '../../assets/images/images'

import { Container, View, Text, Content, Icon} from 'native-base'

export default class LoginContainer extends Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      nameDefault: 'Nombre Guardado',
      hidePassword: true,
      btnDisabled: true,
      email:'',
      password: '',
      passValid: true,
      data:{}
    }
  }
  managePasswordVisibility = () =>{
    this.setState({ hidePassword: !this.state.hidePassword });
  }
  handleChange=(field,value)=>{
    let {data}=this.state
    data[field]=value
  }

  isPassword= (value) => {
    let result = false;
    value? result = true: result= false;
    var re = /^(?=.*\d)(?=.*[\u0021-\u002b\u003c-\u0040])(?=.*[A-Z])(?=.*[a-z])\S{8,16}$/
    validado = value.match(re);

    if (!validado) 
      result =  "";
    return result;
  }

  onChangeText= (value) =>{
    console.log(value)
    this.setState({email:value})
  }
  onBlur= (value)=> {
    // value ? this.state.email : ''
  }
  goRegister= () => {
    this.props.navigation.navigate('Register')
  }

  render() {
    
    let { 
      onChangeText, 
      goRegister,
      onBlur
    } = this

    return (
      <Container style= {[Styles.container, {width:'100%', justifyContent: 'space-around', alignSelf:'center'}]}>
          <Text style={{color:'#898989', textAlign:'center', fontSize: 15, marginTop: 15}}>
            español (Estados Unidos)
          </Text>
          <View  style={{justifyContent:'center', alignItems:'center',  marginTop:'auto'}}>
            <View style={{justifyContent: 'center', alignItems:'center'}}>
              <Image style={{width:200, height: 100, resizeMode: 'contain', alignSelf:'center' }} source={Img.instagramLogo}/>
            </View>
            <View style={{width:'100%', padding:30}}>
              <InputPtimary
                value={this.state.email}
                onChangeText={onChangeText}
                keyboardType={'email-address'}  placeholder={'Ingresa tu email'}
                onBlur={onBlur}  
              >
                
              </InputPtimary>
            <View style = { styles.textBoxBtnHolder }>
              <TextInput 
                value={this.state.password}
                placeholder={'Contraseña'} underlineColorAndroid = "transparent" 
                secureTextEntry = { this.state.hidePassword } style = { styles.textBox }
                onChangeText={password => this.setState({password})}
              />
              
              <TouchableOpacity activeOpacity = { 0.8 } style = { styles.visibilityBtn } onPress = { this.managePasswordVisibility }>
                <Icon name={this.state.hidePassword? 'eye':'eye-off'}/>
              </TouchableOpacity>
            </View>
            {/* {this.state.passValid && 
              <Text style={{color: '#ccc5c5',fontSize: 10,}}>
                La contraseña debe contener al menos un mayúscula, un minúscula, un número y un cracter especial( @, #, $)
              </Text>
            } */}

              <BtnPrimary label= {['Iniciar sesión']} 
                disabled={this.state.email && this.state.password ? false : true}
                btnStyle={this.state.email && this.state.password ? Styles.btnStyle: Styles.btnStyleDisabled }
                onPress={()=>this.props.navigation.navigate('Home')}
              />
              
              <TouchText label={'¿Olvidate tus datos de inicio de sesión?'}
                btnStyle= {Styles.touchStyle} labelStyle={Styles.LabelTouchStyle}>
              </TouchText>
            </View>
          </View>
          <View style= {{width:'100%', alignItems:'center', marginTop:'auto'}}>
            <TouchText onPress={goRegister} label={'¿No tienes una cuenta? Registrate'} btnStyle= {Styles.btnFoot} labelStyle={Styles.LabelTouchStyle}/>
          </View>
       
        
      </Container>
    )
  }
}
const styles = StyleSheet.create(
{

 
  textBoxBtnHolder:
  {
    position: 'relative',
    alignSelf: 'stretch',
    justifyContent: 'center'
  },
  textBox:
  {
    backgroundColor: '#FAFAFA',
    borderRadius: 5,
    borderWidth: 1,
    borderColor:'#ccc5c5',
    marginTop: 20,
    marginBottom: 20,
  },
 
  visibilityBtn:
  {
    position: 'absolute',
    right: 3,
    height: 30,
    width: 30,
  },
 
});