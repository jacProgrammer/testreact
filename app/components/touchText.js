import React from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import { Button,  Text,} from 'native-base'
import Colors from '../assets/style/colors'

const BtnPrimary = ({label, disabled, onPress, btnStyle, labelStyle}) => {
  return ( 
    <TouchableOpacity block 
      disabled={disabled}
      style= {[style.btnStyle, btnStyle]}
      onPress = {onPress}
    >
      <Text style={[style.labelStyle, labelStyle]}>
        {label}
      </Text>
    </TouchableOpacity>
  )
}
const style = StyleSheet.create({
  labelStyle: { 
    color: Colors.SECUNDARY,
    textAlign: 'center'
  },
  btnStyle: {
    width: '100%'
  }
})

export default BtnPrimary