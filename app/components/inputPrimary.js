import React from 'react';
import PropTypes from 'prop-types';
import Colors from '../assets/style/colors'
import { Container,Button, Text} from 'native-base';
import {StyleSheet, TextInput} from 'react-native';
const InputPrimary = ({value, placeholder, keyboardType, styleInput, onChangeText, onBlur}) => {
  return (
    <TextInput
      value= {value}
      placeholder= {placeholder}
      style={[style.labelStyle, styleInput]}
      keyboardType={keyboardType}
      onChangeText={(value) => onChangeText(value)}
      onBlur={(value) => onBlur(value)}
    >
    </TextInput>
  )
}

const style = StyleSheet.create({
  labelStyle: {
    backgroundColor: '#FAFAFA',
    borderRadius: 5,
    borderWidth: 1,
    borderColor:'#ccc5c5'
    
  },
})
export default InputPrimary