import React from 'react';
import {StyleSheet} from 'react-native';
import { Button,  Text,} from 'native-base'
import Colors from '../assets/style/colors'

const BtnPrimary = ({label, disabled, onPress, btnStyle}) => {

  return ( 
    <Button block 
      disabled={disabled}
      style= {[style.btnStyle, btnStyle,]}
      onPress = {onPress}>
      <Text style={style.labelStyle}>
        {label}
      </Text>
    </Button>
  )
}
const style = StyleSheet.create({
  btnStyle: {
    backgroundColor: Colors.SECUNDARY,
    borderColor:'#FFF',
    borderRadius: 5

  },
  labelStyle: { 
    color: Colors.PRIMARY,
    textAlign: 'center',
    fontSize: 11
  }
})

export default BtnPrimary