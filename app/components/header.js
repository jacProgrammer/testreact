
import React from 'react';
import { Header,Button, Icon,Title, Left, Right } from 'native-base';
const HeaderTemplate=({onPress})=>(
  <Header>
    <Left>
      <Button transparent onPress={onPress}>
          <Icon name='menu'/>
      </Button>
    </Left>
    <Title/>
    <Right/>
  </Header>
)
export default HeaderTemplate