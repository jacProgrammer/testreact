import Instagram_logo from './instagram_logo.png'
import ImgTest from './imgTest.jpg'
import ProfileEmpty from './profileEmpty.jpg'
import CardImg from './cardImg.jpg'

const img = {
  instagramLogo : Instagram_logo,
  imgTest : ImgTest,
  profileEmpty: ProfileEmpty,
  cardImg: CardImg
}

export default img